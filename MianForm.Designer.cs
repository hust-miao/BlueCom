﻿namespace BleTest
{
    partial class MianForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.BLE_Scan_Start_Btn = new System.Windows.Forms.Button();
            this.BLE_Scan_Stop_Btn = new System.Windows.Forms.Button();
            this.Connect_Btn = new System.Windows.Forms.Button();
            this.BLE_Device_listView = new System.Windows.Forms.ListView();
            this.BLE_Mac = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.RSSI = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.connect = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.BLE_Name = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Gatt_treeView = new System.Windows.Forms.TreeView();
            this.Log_textBox = new System.Windows.Forms.TextBox();
            this.Send_textBox = new System.Windows.Forms.TextBox();
            this.Send_Btn = new System.Windows.Forms.Button();
            this.HEX_checkBox = new System.Windows.Forms.CheckBox();
            this.Notify_Btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.Clear_Log_Btn = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.RSSI_trackBar = new System.Windows.Forms.TrackBar();
            this.None_Name_checkBox = new System.Windows.Forms.CheckBox();
            this.Name_Filter_textBox = new System.Windows.Forms.TextBox();
            this.RSSI_Lable = new System.Windows.Forms.Label();
            this.Disconnect_Btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.RSSI_trackBar)).BeginInit();
            this.SuspendLayout();
            // 
            // BLE_Scan_Start_Btn
            // 
            this.BLE_Scan_Start_Btn.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BLE_Scan_Start_Btn.Location = new System.Drawing.Point(12, 12);
            this.BLE_Scan_Start_Btn.Name = "BLE_Scan_Start_Btn";
            this.BLE_Scan_Start_Btn.Size = new System.Drawing.Size(81, 29);
            this.BLE_Scan_Start_Btn.TabIndex = 0;
            this.BLE_Scan_Start_Btn.Text = "开始搜索";
            this.BLE_Scan_Start_Btn.UseVisualStyleBackColor = true;
            this.BLE_Scan_Start_Btn.Click += new System.EventHandler(this.BLE_Scan_Start_Btn_Click);
            // 
            // BLE_Scan_Stop_Btn
            // 
            this.BLE_Scan_Stop_Btn.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.BLE_Scan_Stop_Btn.Location = new System.Drawing.Point(99, 12);
            this.BLE_Scan_Stop_Btn.Name = "BLE_Scan_Stop_Btn";
            this.BLE_Scan_Stop_Btn.Size = new System.Drawing.Size(83, 29);
            this.BLE_Scan_Stop_Btn.TabIndex = 0;
            this.BLE_Scan_Stop_Btn.Text = "停止搜索";
            this.BLE_Scan_Stop_Btn.UseVisualStyleBackColor = true;
            this.BLE_Scan_Stop_Btn.Click += new System.EventHandler(this.BLE_Scan_Stop_Btn_Click);
            // 
            // Connect_Btn
            // 
            this.Connect_Btn.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Connect_Btn.Location = new System.Drawing.Point(188, 12);
            this.Connect_Btn.Name = "Connect_Btn";
            this.Connect_Btn.Size = new System.Drawing.Size(84, 29);
            this.Connect_Btn.TabIndex = 0;
            this.Connect_Btn.Text = "发起连接";
            this.Connect_Btn.UseVisualStyleBackColor = true;
            this.Connect_Btn.Click += new System.EventHandler(this.Connect_Btn_Click);
            // 
            // BLE_Device_listView
            // 
            this.BLE_Device_listView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.BLE_Mac,
            this.RSSI,
            this.connect,
            this.BLE_Name});
            this.BLE_Device_listView.FullRowSelect = true;
            this.BLE_Device_listView.GridLines = true;
            this.BLE_Device_listView.HideSelection = false;
            this.BLE_Device_listView.Location = new System.Drawing.Point(12, 78);
            this.BLE_Device_listView.Name = "BLE_Device_listView";
            this.BLE_Device_listView.Size = new System.Drawing.Size(560, 318);
            this.BLE_Device_listView.TabIndex = 1;
            this.BLE_Device_listView.UseCompatibleStateImageBehavior = false;
            this.BLE_Device_listView.View = System.Windows.Forms.View.Details;
            this.BLE_Device_listView.SelectedIndexChanged += new System.EventHandler(this.BLE_Device_listView_SelectedIndexChanged);
            // 
            // BLE_Mac
            // 
            this.BLE_Mac.Text = "MAC地址";
            this.BLE_Mac.Width = 120;
            // 
            // RSSI
            // 
            this.RSSI.Text = "信号强度";
            this.RSSI.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // connect
            // 
            this.connect.Text = "可连接";
            this.connect.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.connect.Width = 50;
            // 
            // BLE_Name
            // 
            this.BLE_Name.Text = "设备名称";
            this.BLE_Name.Width = 315;
            // 
            // Gatt_treeView
            // 
            this.Gatt_treeView.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText;
            this.Gatt_treeView.HideSelection = false;
            this.Gatt_treeView.Location = new System.Drawing.Point(12, 419);
            this.Gatt_treeView.Name = "Gatt_treeView";
            this.Gatt_treeView.Size = new System.Drawing.Size(560, 200);
            this.Gatt_treeView.TabIndex = 2;
            this.Gatt_treeView.DrawNode += new System.Windows.Forms.DrawTreeNodeEventHandler(this.Gatt_treeView_DrawNode);
            // 
            // Log_textBox
            // 
            this.Log_textBox.Location = new System.Drawing.Point(12, 638);
            this.Log_textBox.Multiline = true;
            this.Log_textBox.Name = "Log_textBox";
            this.Log_textBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.Log_textBox.Size = new System.Drawing.Size(560, 205);
            this.Log_textBox.TabIndex = 3;
            // 
            // Send_textBox
            // 
            this.Send_textBox.Location = new System.Drawing.Point(12, 854);
            this.Send_textBox.Name = "Send_textBox";
            this.Send_textBox.Size = new System.Drawing.Size(560, 21);
            this.Send_textBox.TabIndex = 4;
            // 
            // Send_Btn
            // 
            this.Send_Btn.Location = new System.Drawing.Point(497, 889);
            this.Send_Btn.Name = "Send_Btn";
            this.Send_Btn.Size = new System.Drawing.Size(75, 23);
            this.Send_Btn.TabIndex = 5;
            this.Send_Btn.Text = "发送";
            this.Send_Btn.UseVisualStyleBackColor = true;
            this.Send_Btn.Click += new System.EventHandler(this.Send_Btn_Click);
            // 
            // HEX_checkBox
            // 
            this.HEX_checkBox.AutoSize = true;
            this.HEX_checkBox.Location = new System.Drawing.Point(440, 893);
            this.HEX_checkBox.Name = "HEX_checkBox";
            this.HEX_checkBox.Size = new System.Drawing.Size(42, 16);
            this.HEX_checkBox.TabIndex = 6;
            this.HEX_checkBox.Text = "HEX";
            this.HEX_checkBox.UseVisualStyleBackColor = true;
            // 
            // Notify_Btn
            // 
            this.Notify_Btn.Location = new System.Drawing.Point(107, 889);
            this.Notify_Btn.Name = "Notify_Btn";
            this.Notify_Btn.Size = new System.Drawing.Size(75, 23);
            this.Notify_Btn.TabIndex = 5;
            this.Notify_Btn.Text = "Notify";
            this.Notify_Btn.UseVisualStyleBackColor = true;
            this.Notify_Btn.Click += new System.EventHandler(this.Notify_Btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 405);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "服务与特性：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 625);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "收发数据，操作日志：";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label3.Location = new System.Drawing.Point(456, 404);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 12);
            this.label3.TabIndex = 7;
            this.label3.Text = "Designed by 鹏老师";
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Location = new System.Drawing.Point(528, 6);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(53, 12);
            this.linkLabel1.TabIndex = 8;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "项目首页";
            // 
            // Clear_Log_Btn
            // 
            this.Clear_Log_Btn.Location = new System.Drawing.Point(12, 889);
            this.Clear_Log_Btn.Name = "Clear_Log_Btn";
            this.Clear_Log_Btn.Size = new System.Drawing.Size(75, 23);
            this.Clear_Log_Btn.TabIndex = 5;
            this.Clear_Log_Btn.Text = "清空窗口";
            this.Clear_Log_Btn.UseVisualStyleBackColor = true;
            this.Clear_Log_Btn.Click += new System.EventHandler(this.Clear_Log_Btn_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label4.Location = new System.Drawing.Point(285, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(121, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "设备名称不能为空";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.label5.Location = new System.Drawing.Point(10, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(80, 20);
            this.label5.TabIndex = 9;
            this.label5.Text = "RSSI大于：";
            // 
            // RSSI_trackBar
            // 
            this.RSSI_trackBar.Location = new System.Drawing.Point(107, 50);
            this.RSSI_trackBar.Maximum = -40;
            this.RSSI_trackBar.Minimum = -100;
            this.RSSI_trackBar.Name = "RSSI_trackBar";
            this.RSSI_trackBar.Size = new System.Drawing.Size(176, 45);
            this.RSSI_trackBar.TabIndex = 10;
            this.RSSI_trackBar.Value = -40;
            this.RSSI_trackBar.Scroll += new System.EventHandler(this.RSSI_trackBar_Scroll);
            this.RSSI_trackBar.MouseUp += new System.Windows.Forms.MouseEventHandler(this.RSSI_trackBar_MouseUp);
            // 
            // None_Name_checkBox
            // 
            this.None_Name_checkBox.AutoSize = true;
            this.None_Name_checkBox.Checked = true;
            this.None_Name_checkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.None_Name_checkBox.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.None_Name_checkBox.Location = new System.Drawing.Point(404, 49);
            this.None_Name_checkBox.Name = "None_Name_checkBox";
            this.None_Name_checkBox.Size = new System.Drawing.Size(59, 24);
            this.None_Name_checkBox.TabIndex = 11;
            this.None_Name_checkBox.Text = ",包含";
            this.None_Name_checkBox.UseVisualStyleBackColor = true;
            this.None_Name_checkBox.CheckedChanged += new System.EventHandler(this.None_Name_checkBox_CheckedChanged);
            // 
            // Name_Filter_textBox
            // 
            this.Name_Filter_textBox.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name_Filter_textBox.Location = new System.Drawing.Point(458, 48);
            this.Name_Filter_textBox.Name = "Name_Filter_textBox";
            this.Name_Filter_textBox.Size = new System.Drawing.Size(113, 26);
            this.Name_Filter_textBox.TabIndex = 13;
            this.Name_Filter_textBox.TextChanged += new System.EventHandler(this.Name_Filter_textBox_TextChanged);
            // 
            // RSSI_Lable
            // 
            this.RSSI_Lable.AutoSize = true;
            this.RSSI_Lable.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.RSSI_Lable.Location = new System.Drawing.Point(79, 50);
            this.RSSI_Lable.Margin = new System.Windows.Forms.Padding(0);
            this.RSSI_Lable.Name = "RSSI_Lable";
            this.RSSI_Lable.Size = new System.Drawing.Size(39, 20);
            this.RSSI_Lable.TabIndex = 14;
            this.RSSI_Lable.Text = "-100";
            // 
            // Disconnect_Btn
            // 
            this.Disconnect_Btn.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Disconnect_Btn.Location = new System.Drawing.Point(278, 12);
            this.Disconnect_Btn.Name = "Disconnect_Btn";
            this.Disconnect_Btn.Size = new System.Drawing.Size(84, 29);
            this.Disconnect_Btn.TabIndex = 0;
            this.Disconnect_Btn.Text = "断开连接";
            this.Disconnect_Btn.UseVisualStyleBackColor = true;
            this.Disconnect_Btn.Click += new System.EventHandler(this.Disconnect_Btn_Click);
            // 
            // MianForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(584, 921);
            this.Controls.Add(this.RSSI_Lable);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.Name_Filter_textBox);
            this.Controls.Add(this.None_Name_checkBox);
            this.Controls.Add(this.BLE_Device_listView);
            this.Controls.Add(this.RSSI_trackBar);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.HEX_checkBox);
            this.Controls.Add(this.Clear_Log_Btn);
            this.Controls.Add(this.Notify_Btn);
            this.Controls.Add(this.Send_Btn);
            this.Controls.Add(this.Send_textBox);
            this.Controls.Add(this.Log_textBox);
            this.Controls.Add(this.Gatt_treeView);
            this.Controls.Add(this.Disconnect_Btn);
            this.Controls.Add(this.Connect_Btn);
            this.Controls.Add(this.BLE_Scan_Stop_Btn);
            this.Controls.Add(this.BLE_Scan_Start_Btn);
            this.MaximizeBox = false;
            this.Name = "MianForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BlueCom (低功耗蓝牙调试助手) V0.1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RSSI_trackBar)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BLE_Scan_Start_Btn;
        private System.Windows.Forms.Button BLE_Scan_Stop_Btn;
        private System.Windows.Forms.Button Connect_Btn;
        private System.Windows.Forms.ColumnHeader BLE_Mac;
        private System.Windows.Forms.ColumnHeader RSSI;
        private System.Windows.Forms.ColumnHeader connect;
        private System.Windows.Forms.ColumnHeader BLE_Name;
        public System.Windows.Forms.ListView BLE_Device_listView;
        private System.Windows.Forms.TreeView Gatt_treeView;
        private System.Windows.Forms.TextBox Log_textBox;
        private System.Windows.Forms.TextBox Send_textBox;
        private System.Windows.Forms.Button Send_Btn;
        private System.Windows.Forms.CheckBox HEX_checkBox;
        private System.Windows.Forms.Button Notify_Btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Button Clear_Log_Btn;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TrackBar RSSI_trackBar;
        private System.Windows.Forms.CheckBox None_Name_checkBox;
        private System.Windows.Forms.TextBox Name_Filter_textBox;
        private System.Windows.Forms.Label RSSI_Lable;
        private System.Windows.Forms.Button Disconnect_Btn;
    }
}

